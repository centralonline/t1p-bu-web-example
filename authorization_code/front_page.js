// base T1P service
var base_uri = 'https://auth.the-1-card.com';

var setElementText = function(id, v) { document.getElementById(id).innerText = v }

main();

// ----- functions ----

function main() {
  var token_cookie = getCookie('token')
  console.log('===================')
  console.log(token_cookie)
  if (token_cookie){
    var token = JSON.parse(token_cookie)
    var claims = JSON.parse(getCookie('claims'))
    setElementText('claims', JSON.stringify(claims, null, 2))
    setElementText('access_token', token.access_token)
    setElementText('id_token', token.id_token)
    setElementText('expiration', token.expires_in)
    setElementText('token_type', token.token_type)
    $(".chatbot").append('<a href="/example/chatbot?access_token='+claims.customer_mobile_id+'">Connect Chatbot</a>')
  } else
    window.location.href = '/example/login'
}

// code from https://www.w3schools.com/js/js_cookies.asp
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ')
          c = c.substring(1);
      if (c.indexOf(name) == 0)
          return c.substring(name.length, c.length);
  }
  return "";
}
