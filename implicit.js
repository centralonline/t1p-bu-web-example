'use strict';
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const promise = require('bluebird')
const https = require('https')
const http = require('http')
const fs = promise.promisifyAll(require("fs"))
const url = require('url')
const path = require('path')
const request = require('request')
const nunjucks = require('nunjucks')

// The port that this example is going to run.
const port = 5000

// base T1P uat service
//const base_uri = 'https://uat-auth.the-1-card.com'
// base T1P prod service
const base_uri = 'https://auth.the-1-card.com'

// OpenID endpoints. Note that these endpoints can be disconvered from https://uat_authenticate.the-1-card.com/authenservice/op/.well-known/openid-configuration
// See also: http://openid.net/specs/openid-connect-discovery-1_0.html
//
const base_auth_url = `${base_uri}/authenservice/op`
const url_auth      = `${base_auth_url}/auth`
const url_token     = `${base_auth_url}/token`
const url_userinfo  = `${base_auth_url}/me`
const url_revoke_access_token = `${base_auth_url}/token/revocation`
const url_logout    = `${base_auth_url}/session/end`

// -- uat url_bu
//const url_bu        = `https://uat-auth.the-1-card.com/example`
// -- prod url_bu
const url_bu        = `https://auth.the-1-card.com/example`
// -- test url_bu
//const url_bu        = `http://t1p.com:${port}`
const redirect_uri  = `${url_bu}/auth`
const register_callback_uri = `${url_bu}/after_register`
// client_id and client_secret are generated from T1P admin and sent to each BU.
// -- uat client_id
//const client_id     = 'b6e3effd-265e-4118-ac78-9c720672df84'
//const client_secret = 's0YXx1mZN1KQQxxjvg9DBZii+NCM33znw5nRAV4aviE'
// -- prod client_id
const client_id     = 'a9dc11ae-2d42-4088-8c06-ddd0c312e16c'
const client_secret = 'adfc1149-98cc-4652-aed3-dfa96ef224d8'

// valid scopes. note `openid` is a must.
const scope = 'openid profile customer_mobile_id'

const basicAuth = new Buffer(`${client_id}:${client_secret}`).toString('base64');

let app = express()
app.engine('html', nunjucks.render);
app.set('view engine', 'html');
const env = nunjucks.configure([__dirname + '/views'], {
    autoescape: true,
    watch: true,
    noCache: true,
    express: app
});

app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.set('view cache', false);
app.use(express.static(path.join(__dirname, 'public')));
app.disable('x-powered-by')
app.set('trust proxy', true)
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: false, limit: '100kb' }))
let router = express.Router()
router.use('/authorization_code', express.static('authorization_code'))
router.use('/docs', express.static('docs'))
router.use('/images', express.static('images'))

//    FUNCTION    ///////////////////////////////////////

const successResult = value => ({ success: true, value })
const failureResult = value => ({ success: false, value })

const renderFromFile = (res, p) => res.sendFile(path.join(__dirname+p))

const renderHTML = (res, next, html_file, target, content) =>
   fs.readFileAsync(html_file, 'utf8')
     .then(html => html.replace(target, content))
     .then(html => res.send(html))
     .catch(next)

//    ROUTE    ///////////////////////////////////////


router.get('/', (_, res) => renderFromFile(res, '/authorization_code/front_page.html'))

router.get('/login', (req, res, next) => {
  // state is a value that you use for tracking login flow. 
  // You should ensure it is the same value when you get called back.
  let state = '123456'

  // Nonce string for preventing replay attach. See http://openid.net/specs/openid-connect-core-1_0.html#ImplicitAuthRequest
  let nonce = 'thisshouldbealongrandomgeneratedstringtopreventreplayattack'

  return renderHTML(res, next, 'layout.html', '<!-- content -->', `<div><a class='btn_login' href="${url_auth}?response_type=id_token%20token&client_id=${client_id}&state=${state}&nonce=${nonce}&redirect_uri=${redirect_uri}&scope=${encodeURI(scope)}&register_callback_url=${register_callback_uri}"><img src="images/logo-t-1-c-1.png" srcset="images/logo-t-1-c-1@2x.png 2x, images/logo-t-1-c-1@3x.png 3x" class="logo_t1c"/><span>LOGIN WITH The 1 Card</span></a></div>
  <div><a href="/example/docs/index.html">API Reference</a></div>`)
})

router.get('/logout', (req, res, next) => {
  const jsonToken = req.cookies.token
  if (jsonToken){
    const token = JSON.parse(jsonToken)
    res.clearCookie('token');
    res.clearCookie('claims');

    const params = {
      url: url_revoke_access_token,
      headers: {
        Authorization: `Basic ${basicAuth}`
      },
      json: true,
      form: { 
        token: token.access_token, 
        token_type_hint: token.token_type
      }
    }

    request.post(params, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        return res.redirect(`${url_logout}?id_token_hint=${token.id_token}&post_logout_redirect_uri=${url_bu}/doc`)
      }else{
        next(new Error(body.error_description || "unknown error."))
      }
    })
  } else return res.redirect(url_bu)
})

router.get('/doc', (req, res, next) => {
  res.redirect('/example/doc/login_oauth')
})

router.get('/auth', (req, res, next) => renderFromFile(res, '/implicit.content.html'))

router.get('/doc/login/overview', (req, res, next) => {
  res.render('doc', {
    url_login:url_login,
    url_login_profile:url_login_profile,
    url_login_profile_customer_mobile_id:url_login_profile_customer_mobile_id,
    url:url,
    client:{
      base_uri: base_uri,
      client_id: client_id,
      client_secret: client_secret,
      state: state,
      scope: scope,
      redirect_uri: redirect_uri,
      register_callback_uri: register_callback_uri
    }
  });
})

router.get('/doc/login_oauth', (req, res, next) => {
  const state = '123456'
  // Nonce string for preventing replay attach. See http://openid.net/specs/openid-connect-core-1_0.html#ImplicitAuthRequest
  let nonce = 'thisshouldbealongrandomgeneratedstringtopreventreplayattack'
  const url = `${url_auth}?response_type=id_token%20token&client_id=${client_id}&state=${state}&nonce=${nonce}&redirect_uri=${redirect_uri}&scope=${encodeURI(scope)}&register_callback_url=${register_callback_uri}`;
  const url_login = `${url_auth}?response_type=id_token%20token&client_id=${client_id}&state=${state}&nonce=${nonce}&redirect_uri=${redirect_uri}&scope=${encodeURI("openid")}`;
  const url_login_profile = `${url_auth}?response_type=id_token%20token&client_id=${client_id}&state=${state}&nonce=${nonce}&redirect_uri=${redirect_uri}&scope=${encodeURI("openid profile")}`;
  const url_login_profile_customer_mobile_id = `${url_auth}?response_type=id_token%20token&client_id=${client_id}&state=${state}&nonce=${nonce}&redirect_uri=${redirect_uri}&scope=${encodeURI("openid profile customer_mobile_id")}`;

  res.render('./accounts/login_oauth', {
  url_login:url_login,
  url_login_profile:url_login_profile,
  url_login_profile_customer_mobile_id:url_login_profile_customer_mobile_id,
  url:url,
  client:{
    base_uri: base_uri,
    client_id: client_id,
    client_secret: client_secret,
    state: state,
    scope: scope,
    redirect_uri: redirect_uri,
    register_callback_uri: register_callback_uri
  }
})})

router.get('/doc/login_oauth_chatbot', (req, res, next) => {
  const state = '123456'
  // Nonce string for preventing replay attach. See http://openid.net/specs/openid-connect-core-1_0.html#ImplicitAuthRequest
  let nonce = 'thisshouldbealongrandomgeneratedstringtopreventreplayattack'
  const url = `${url_auth}?response_type=id_token%20token&client_id=${client_id}&state=${state}&nonce=${nonce}&redirect_uri=${redirect_uri}&scope=${encodeURI(scope)}&register_callback_url=${register_callback_uri}`;
  const url_login = `${url_auth}?response_type=id_token%20token&client_id=${client_id}&state=${state}&nonce=${nonce}&redirect_uri=${redirect_uri}&scope=${encodeURI("openid")}`;
  const url_login_profile = `${url_auth}?response_type=id_token%20token&client_id=${client_id}&state=${state}&nonce=${nonce}&redirect_uri=${redirect_uri}&scope=${encodeURI("openid profile")}`;
  const url_login_profile_customer_mobile_id = `${url_auth}?response_type=id_token%20token&client_id=${client_id}&state=${state}&nonce=${nonce}&redirect_uri=${redirect_uri}&scope=${encodeURI("openid profile customer_mobile_id")}`;

  res.render('./accounts/login_oauth_chatbot', {
  url_login:url_login,
  url_login_profile:url_login_profile,
  url_login_profile_customer_mobile_id:url_login_profile_customer_mobile_id,
  url:url,
  client:{
    base_uri: base_uri,
    client_id: client_id,
    client_secret: client_secret,
    state: state,
    scope: scope,
    redirect_uri: redirect_uri,
    register_callback_uri: register_callback_uri
  }
})})


// const state = '123456'
// dev
// const config_doc = {
//   swagger_url : 'http://127.0.0.1:1337/swagger/api-docs',
//   host : 'q68gfk04c8.execute-api.ap-southeast-1.amazonaws.com/staging',
// }
// uat
const config_doc = {
  swagger_url : 'https://q68gfk04c8.execute-api.ap-southeast-1.amazonaws.com/dev/swagger/api-docs',
  host : 'q68gfk04c8.execute-api.ap-southeast-1.amazonaws.com/staging',
}

router.get('/doc/overview', (req, res, next) => {
  res.render('./mains/overview', {
  swagger_url : config_doc.swagger_url
})})
router.get('/doc/login_api', (req, res, next) => {
  res.render('./accounts/login_api', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/token',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_token',
    name: 'Login with Api'
  })
})
router.get('/doc/create_account', (req, res, next) => {
  res.render('./accounts/create_account',  {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/accounts',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_accounts',
    name: 'Create Customer Account'
  })
})
router.get('/doc/create_online_account', (req, res, next) => {
  res.render('./accounts/create_online_account', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/activation',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_activation',
    name: 'Create Online Account'
  })
})
router.get('/doc/send_otp', (req, res, next) => {
  res.render('./accounts/send_otp', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/notification/otp',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_notification_otp',
    name: 'Send OTP'
  })
})
router.get('/doc/verify_otp', (req, res, next) => {
  res.render('./accounts/verify_otp', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/notification/otp/{request_id}/verification',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_notification_verify',
    name: 'Verify OTP'
  })
})
router.get('/doc/forgot_pin', (req, res, next) => {
  res.render('./accounts/forgot_pin', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/forgotpin',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_forgot_pin',
    name: 'Forgot Pin'
  })
})
router.get('/doc/reset_pin', (req, res, next) => {
  res.render('./accounts/reset_pin', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/resetpin/{forgot_pin_id}',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_reset_pin',
    name: 'Reset Pin'
  })
})
router.get('/doc/forgot_password', (req, res, next) => {
  res.render('./accounts/forgot_password', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/forgotpassword',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_forgot_password',
    name: 'Forgot Password'
  })
})
router.get('/doc/update_mobile_login', (req, res, next) => {
  res.render('./accounts/update_mobile_login', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'PUT',
    api_url: '/v2/t1c/mobile',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'put_mobile',
    name: 'Update Mobile Login'
  })
})
router.get('/doc/logout', (req, res, next) => {
  res.render('./accounts/logout', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'DELETE',
    api_url: '/v2/t1c/token',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'delete_mobile',
    name: 'Logout'
  })
})
router.get('/doc/check_access_token', (req, res, next) => {
  res.render('./accounts/check_access_token', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'GET',
    api_url: '/v2/t1c/token/{customer_access_token}',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'get_mobile',
    name: 'Check Access Token'
  })
})
router.get('/doc/verify_pin', (req, res, next) => {
  res.render('./accounts/verify_pin', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/verify_pin',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_verify_pin',
    name: 'Verify Pin'
  })
})
router.get('/doc/check_profile_customer', (req, res, next) => {
  res.render('./accounts/check_profile_customer', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/accounts',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'get_accounts',
    name: 'Check Profile Customer'
  })
})
router.get('/doc/request_account', (req, res, next) => {
  res.render('./accounts/request_account', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/request_account',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_request_account',
    name: 'Request Create Account'
  })
})
router.get('/doc/verify_account', (req, res, next) => {
  res.render('./accounts/verify_account', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/request_account/{request_register_id}/verification',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'post_request_account_verification',
    name: 'Request Create Account Verify'
  })
})
router.get('/doc/refresh_customer_access_token', (req, res, next) => {
  res.render('./accounts/refresh_customer_access_token', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'PUT',
    api_url: '/v2/t1c/token',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'put_token',
    name: 'Refresh Customer Access Token'
  })
})

router.get('/doc/change_mobile_redemption', (req, res, next) => {
  res.render('./accounts/change_mobile_redemption', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'PUT',
    api_url: '/v2/t1c/change_mobile_redemption',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'change_mobile_redemption',
    name: 'Change Mobile Redemption'
  })
})
router.get('/doc/get_customer', (req, res, next) => {
  res.render('./accounts/get_customer', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'GET',
    api_url: '/v2/t1c/customer/:customer_access_token',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'get_customer',
    name: 'Get Customer Info'
  })
})
router.get('/doc/request_redemption', (req, res, next) => {
  res.render('./accounts/request_redemption', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/request_redemption',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'request_redemption',
    name: 'Request Redempton (OTP)'
  })
})
router.get('/doc/verify_redemption', (req, res, next) => {
  res.render('./accounts/verify_redemption', {
    swagger_url: config_doc.swagger_url,
    host: config_doc.host,
    api_method: 'POST',
    api_url: '/v2/t1c/verify_redemption',
    example_req: JSON.stringify({}),
    example_res: JSON.stringify({}),
    swagger_action: 'verify_redemption',
    name: 'Request Redempton (OTP)'
  })
})

router.get('/the1card_redirect', (req, res, next) => renderHTML(res, next, 'implicit.content.html', `window.location.replace(url_bu)`, ``))

router.use((error, req, res, next) => res.send(error.message))
app.use('/example', router)

// app.listen(port)
// console.log(`start server at: ${url_bu}`)
const server = app.listen(process.env.PORT || 5000, () => {
  console.log(`start server at: ${url_bu}`)
});

const io = require('socket.io')(server);
var options = {
  sessionId: 'cebd73c8ba8a4dbd8a35fc2f14473a70',
};

var numUsers = 0;
var socket;
var customer_access_token;

io.on('connection', function (socket) {
  var addedUser = false;

  socket.on('new message', function (data) {
    if(!socket.username)
      socket.username = customer_access_token
    console.log('user:'+socket.username+'|'+data)
    var request = apiai.textRequest('user:'+socket.username+'|'+data, options);
    request.on('response', function(response) {
        socket.emit('bot reply', response.result.fulfillment.messages[0].speech);
    });

    request.on('error', function(error) {
        console.log(error);
    });

    request.end();

  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', function (username) {
    if (addedUser) return;

    console.log('add user',socket.username)
    // we store the username in the socket session for this client
    socket.username = username;
    ++numUsers;
    addedUser = true;

  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    if (addedUser) {
      --numUsers;

      // echo globally that this client has left
      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers: numUsers
      });
    }
  });
});

const apiai = require('apiai')('308ac7bd1b7e4caf912b78e4f2a89619');

router.get('/chatbot', (req, res, next) => {
  
  customer_access_token = req.query.access_token ? req.query.access_token : null
  // console.log('req.cookies.customer_access_token',req.cookies.cookieName)
  // // socket.emit('add user', customer_access_token);
  // res.cookie('customer_access_token', customer_access_token, { maxAge: 900000, httpOnly: false });

  var cookie = req.cookies.customer_access_token;
  res.cookie('customer_access_token', customer_access_token , { maxAge: 900000, httpOnly: true });
    console.log('cookie created successfully');
    console.log('cookie exists', cookie);
  
  //next(); // <-- important!

  res.render('./demo/chatbot', {
    
  })
})

router.get('/chatbot/msg', (req, res, next) => {
  // if(!socket.username)
  //   socket.username = customer_access_token
  var data = req.query.data
  console.log('user:'+req.cookies.customer_access_token+'|'+data)
  var contexts = [
    {
        name: 'customer_access_token',
        parameters: {
            'user': req.cookies.customer_access_token
        },
        lifespan: 1
    }
  ]
  options.contexts = contexts
  console.log('options', options)
  var request = apiai.textRequest(data, options);
  // var request = apiai.textRequest('user:'+req.cookies.customer_access_token+'|'+data, options);
  request.on('response', function(response) {
    if(response.result.fulfillment.messages){
      console.log('response',response.result.fulfillment.messages[0])
      res.send(response.result.fulfillment.messages[0].speech);
    } else {
      res.send('... I dont know.');
    }
  });

  request.on('error', function(error) {
      console.log(error);
  });

  request.end();
})
