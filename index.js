'use strict';
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const promise = require('bluebird')
const fs = promise.promisifyAll(require("fs"));
const url = require('url')
const path = require('path')
const request = require('request')
const nunjucks = require('nunjucks');

// The port that this example is going to run.
const port = 3000

// base T1P service
const base_uri = 'https://uat-auth.the-1-card.com'

// OpenID endpoints. Note that these endpoints can be disconvered from http://uat_authenticate.the-1-card.com/authenservice/op/.well-known/openid-configuration
// See also: http://openid.net/specs/openid-connect-discovery-1_0.html
//
const base_auth_url = `${base_uri}/authenservice/op`
const url_auth      = `${base_auth_url}/auth`
const url_token     = `${base_auth_url}/token`
const url_userinfo  = `${base_auth_url}/me`
const url_revoke_access_token = `${base_auth_url}/token/revocation`
const url_logout    = `${base_auth_url}/session/end`

// URL of this example BU.
// const url_bu        = `http://t1p.com:${port}`
const url_bu        = `https://uat-auth.the-1-card.com/example`
const redirect_uri  = `${url_bu}/auth`
const register_callback_uri = `${url_bu}/after_register`

// client_id and client_secret are generated from T1P admin and sent to each BU.
//
const client_id     = 'b6e3effd-265e-4118-ac78-9c720672df84'
const client_secret = 's0YXx1mZN1KQQxxjvg9DBZii+NCM33znw5nRAV4aviE'

// valid scopes. note `openid` is a must.
const scope         = 'openid profile customer_mobile_id'

const basicAuth = new Buffer(`${client_id}:${client_secret}`).toString('base64');

let app = express()

app.engine('html', nunjucks.render);
app.set('view engine', 'html');
const env = nunjucks.configure([__dirname + '/views'], {
    autoescape: true,
    watch: true,
    noCache: true,
    express: app
});

app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.set('view cache', false);
app.use(express.static(path.join(__dirname, 'public')));
app.disable('x-powered-by')
app.set('trust proxy', true)
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: false, limit: '100kb' }))
app.use('/authorization_code', express.static('authorization_code'))
app.use('/docs', express.static('docs'))
app.use('/images', express.static('images'))

//    FUNCTION    ///////////////////////////////////////

const successResult = value => ({ success: true, value })
const failureResult = value => ({ success: false, value })

const renderFromFile = (res, p) => res.sendFile(path.join(__dirname+p))

const renderHTML = (res, next, content) =>
   fs.readFileAsync('layout.html', 'utf8')
     .then(html => html.replace('<!-- content -->', content))
     .then(html => res.send(html))
     .catch(next)

const decodeJwtPart = part => JSON.parse(new Buffer(part, 'base64').toString('ascii'))

function validateIDToken(id_token) {
  // split claims out of ID Token according to OpenID Core spec (http://openid.net/specs/openid-connect-core-1_0.html#id_tokenExample)
  let arr = id_token.split('.')

  if (arr.length !== 3)
    return failureResult(new Error('id_token fail to validation.'))

  let claims = decodeJwtPart(arr[1])
  
  const errors = ['iss', 'sub', 'aud', 'exp', 'iat']
                          .filter(field => claims[field] === undefined)
                          .map(field => `missing required JWT property ${field}`)
  
  let current_time = Math.round((new Date()).getTime()/1000)
  const CLOCK_TOLERANCE = 5
  if (claims.iss !== base_auth_url) errors.push('id_token unexpected issuer')
  if (claims.aud !== client_id) errors.push('id_token unexpected client_id')
  if (claims.exp < current_time) errors.push('id_token expired')
  if (claims.iat > (current_time + CLOCK_TOLERANCE)) errors.push('id_token issued in the future')

  return errors.length > 0
          ? failureResult(new Error(errors.join('\n')))
          : successResult(claims)
}

//    ROUTE    ///////////////////////////////////////

app.get('/', (_, res) => renderFromFile(res, '/authorization_code/front_page.html'))

app.get('/login', (req, res, next) => {
  // state is a value that you use for tracking login flow. 
  // You should ensure it is the same value when you get called back.
  const state = '123456'

  return renderHTML(res, next, `
    <div>
      <a class='btn_login' href="${url_auth}?response_type=code&client_id=${client_id}&state=${state}&redirect_uri=${redirect_uri}&scope=${encodeURI(scope)}&register_callback_url=${register_callback_uri}">
        <img src="images/logo-t-1-c-1.png" srcset="images/logo-t-1-c-1@2x.png 2x, images/logo-t-1-c-1@3x.png 3x" class="logo_t1c"/>
        <span>LOGIN WITH The 1 Card</span>
      </a>
    </div>
  <div><a href="/docs/index.html">API Reference</a></div>`)
})

app.get('/example/logout', (req, res, next) => {
  const jsonToken = req.cookies.token
  if (jsonToken){
    const token = JSON.parse(jsonToken)
    res.clearCookie('token');
    res.clearCookie('claims');

    const params = {
      url: url_revoke_access_token,
      headers: {
        Authorization: `Basic ${basicAuth}`
      },
      json: true,
      form: { 
        token: token.access_token, 
        token_type_hint: token.token_type
      }
    }

    request.post(params, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        return res.redirect(`${url_logout}?id_token_hint=${token.id_token}&post_logout_redirect_uri=${url_bu}`)
      }else{
        next(new Error(body.error_description || "unknown error."))
      }
    })
  }else
    return res.redirect(url_bu)
})

app.get('/auth', (req, res, next) => {
  const params = {
    url: url_token,
    headers: {
      Authorization: `Basic ${basicAuth}`
    },
    json: true,
    form: { 
      code: req.query.code, 
      grant_type: "authorization_code",
      redirect_uri: redirect_uri
    }
  }
  
  request.post(params, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      console.log('========== start token ===========')
      console.log('id_token = ', body.id_token)
      console.log('access_token = ', body.access_token)
      console.log('expires_in = ', body.expires_in)
      console.log('token_type = ', body.token_type)
      console.log('=========== end token ============')
      
      const validatedResult = validateIDToken(body.id_token)
      if (validatedResult.success){
        const claims = validatedResult.value
        res.cookie('token' , JSON.stringify(body));
        res.cookie('claims' , JSON.stringify(claims));
        return renderHTML( res, next,
        `<script type="text/javascript">
        window.location.replace('${url_bu}');
        </script>
        `)
      } else
        next(validatedResult.value)
    } else {
      const message = `Token exchange failed: ${body && body.error_description?  body.error_description : "unknown error."}`
      next(new Error(message))
    }
  })
})

app.get('/doc', (req, res, next) => {
  // state is a value that you use for tracking login flow. 
  // You should ensure it is the same value when you get called back.
  const state = '123456'
  // res.sendFile(path.join(__dirname+'/doc.html'));
  const url = `${url_auth}?response_type=code&client_id=${client_id}&state=${state}&redirect_uri=${redirect_uri}&scope=${encodeURI(scope)}&register_callback_url=${register_callback_uri}`;
  const url_login = `${url_auth}?response_type=code&client_id=${client_id}&state=${state}&redirect_uri=${redirect_uri}&scope=${encodeURI("openid")}`;
  const url_login_profile = `${url_auth}?response_type=code&client_id=${client_id}&state=${state}&redirect_uri=${redirect_uri}&scope=${encodeURI("openid profile")}`;
  const url_login_profile_customer_mobile_id = `${url_auth}?response_type=code&client_id=${client_id}&state=${state}&redirect_uri=${redirect_uri}&scope=${encodeURI("openid profile customer_mobile_id")}`;
  
  res.render('doc', {
    url_login:url_login,
    url_login_profile:url_login_profile,
    url_login_profile_customer_mobile_id:url_login_profile_customer_mobile_id,
    url:url,
    client:{
      base_uri: base_uri,
      client_id: client_id,
      client_secret: client_secret,
      state: state,
      scope: scope,
      redirect_uri: redirect_uri,
      register_callback_uri: register_callback_uri
    }
  });
  // return renderHTML(res, next, `<div><a class='btn_login' href="${url_auth}?response_type=code&client_id=${client_id}&state=${state}&redirect_uri=${redirect_uri}&scope=${encodeURI(scope)}&register_callback_url=${register_callback_uri}"><img src="images/logo-t-1-c-1.png" srcset="images/logo-t-1-c-1@2x.png 2x, images/logo-t-1-c-1@3x.png 3x" class="logo_t1c"/><span>LOGIN WITH The 1 Card</span></a></div>
  // <div><a href="/docs/index.html">API Reference</a></div>`)
})

app.use((error, req, res, next) => res.send(error.message))
app.listen(port)
console.log(`start server at: ${url_bu}`)