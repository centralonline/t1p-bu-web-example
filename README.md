# The 1 Project OpenID Connect example

This repository is a sample website that consumes T1P Authenticaiton service with OpenID Connect. The website is run on NodeJS.  This example expects modern browsers (HTML5 and ES6).

This example shows how to use OpenID Connect's Authorization flow to acquire an access token (OAuth 2) and an ID token (OpenID).
It begins from a front page with a simple "Connect to T1P" link and then progresses through login processes.  Once it gets tokens,
both tokens are kept in browser's cookie storage and the browser is redirected to the front page again.  This time front page should
show user information extracted from the ID token retrieved from the cookie storage.

## Installation Guide

### Pre-requisites

1. Install NodeJS version 6+ from https://nodejs.org/en/
2. Map name `t1p.com` to `127.0.0.1` (localhost) in your system host file: `/etc/hosts` (Linux/Mac) or `C:\Windows\system32\drivers\etc\hosts` (Windows).
   * Note, this step needs administration privilege

### Run sample web site

1. At the root of this repository, run `npm install`.
2. Run server with command `node authorization_flow` for OpenID's Authorization flow example.
  - or Run `node implicit` for OpenID's Implicit flow example.

You should be able to access the sample website through http://t1p.com:3000/

## API Reference

See `./docs/index.html`.

## Add Monitor